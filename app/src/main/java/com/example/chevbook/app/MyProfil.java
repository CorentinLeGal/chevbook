package com.example.chevbook.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MyProfil extends ActionBarActivity {

    private profile mProfile;
    private AsyncTask<String, String, Boolean> mthreaduser;
    private SharedPreferences myPrefs;

    private TextView nom;
    private TextView nomf;
    private TextView prenom;
    private TextView classe;
    private TextView sexe;
    private TextView age;
    private TextView mail;
    private TextView telephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profil);

        getActionBar().setTitle("Mon Profil");

        final ImageButton Button = (ImageButton) findViewById(R.id.btneditprofil);
        Button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyProfil.this, EditProfil.class);
                startActivity(intent);
            }
        });

        nom = (TextView)findViewById(R.id.TVpseudoprofil);
        nomf = (TextView)findViewById(R.id.TVnomprofil);
        prenom = (TextView)findViewById(R.id.TVprenomprofil);
        classe = (TextView)findViewById(R.id.TVclasseprofil);
        sexe = (TextView)findViewById(R.id.TVsexeprofil);
        age = (TextView)findViewById(R.id.TVageprofil);
        mail = (TextView)findViewById(R.id.TVmailprofil);
        telephone = (TextView)findViewById(R.id.TVtelprofil);


        myPrefs = getSharedPreferences("infoconnexion", 0);

        String[] mesparams = {"http://chevbook.charrierpaul.fr/androInfoProfil.php", myPrefs.getString("snom", "nothing")};
        mthreaduser = new profileCo(MyProfil.this).execute(mesparams);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.my_profil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setDataProfil(String sb) {

        try {
            JSONArray jsonArray = new JSONArray(sb);
            JSONObject jsonObject = jsonArray.getJSONObject(0);


            mProfile = new profile(jsonObject.getString("nom"),jsonObject.getString("nomF"),jsonObject.getString("prenom"),jsonObject.getString("classe"),jsonObject.getString("sexe"),jsonObject.getString("age"),jsonObject.getString("mail"),jsonObject.getString("tel") );

            nom.setText(mProfile.getNom());
            nomf.setText(mProfile.getNomF());
            prenom.setText(mProfile.getPrenom());
            classe.setText(mProfile.getClasse());
            sexe.setText(mProfile.getSexe());
            age.setText(mProfile.getAge());
            mail.setText(mProfile.getMail());
            telephone.setText(mProfile.getTelephone());



        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
