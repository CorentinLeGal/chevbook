package com.example.chevbook.app;

 import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chevbook.app.R;

import java.text.SimpleDateFormat;
import java.util.List;

public class ListViewAppartAdapter extends BaseAdapter {

    private List<Annonce> list;
    private final Context _c;

    public ListViewAppartAdapter(Context context) {
        this._c = context;
        this.list = null;
    }

    public ListViewAppartAdapter(Context context, List<Annonce> listAnnonces) {
        this._c = context;
        this.list = listAnnonces;
    }

    private static class ViewHolder {
        public TextView title;
        public ImageView image;
        public TextView adresse;
        public TextView prix;
        public TextView colloc;
        public TextView surface;
        public TextView meuble;
        public TextView distance;
        public TextView internet;
        public TextView nbpiece;
        public TextView parking;
        public TextView presentation;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return list.size();
        //return 8;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder = null;
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.row_item_appart, null);
            holder = new ViewHolder();

            holder.title = (TextView) v.findViewById(R.id.TVtitreannonce);
            /*holder.adresse = (TextView) v.findViewById(R.id.TVadr);
            holder.prix = (TextView) v.findViewById(R.id.TVprix);
            holder.colloc = (TextView) v.findViewById(R.id.TVcolloc);
            holder.surface = (TextView) v.findViewById(R.id.TVsurface);
            holder.meuble = (TextView) v.findViewById(R.id.TVmeuble);
            holder.distance = (TextView) v.findViewById(R.id.TVdist);
            holder.internet = (TextView) v.findViewById(R.id.TVnet);
            holder.nbpiece = (TextView) v.findViewById(R.id.TVnbpiece);
            holder.parking = (TextView) v.findViewById(R.id.TVparking);
            holder.image = (ImageView) v.findViewById(R.id.imageViewappart);*/
            holder.presentation = (TextView) v.findViewById(R.id.TVpresentation);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }


        holder.title.setText(list.get(position).getNom());
        holder.presentation.setText(list.get(position).getPresent());
        /*holder.nbpiece.setText(Integer.toString(list.get(position).getNbPiece()));
        holder.surface.setText(Integer.toString(list.get(position).getSurface()));
        holder.adresse.setText(list.get(position).getAdresse());
        holder.prix.setText(Integer.toString(list.get(position).getPrix()));
        holder.distance.setText(Integer.toString(list.get(position).getDistanceLycee()));

        if (list.get(position).isCollocation()){
            holder.colloc.setText("Oui");
        }else {
            holder.colloc.setText("Non");
        }

        if (list.get(position).isMeuble()){
            holder.meuble.setText("Oui");
        }else {
            holder.meuble.setText("Non");
        }

        if (list.get(position).isInternet()){
            holder.internet.setText("Oui");
        }else {
            holder.internet.setText("Non");
        }

        if (list.get(position).isParking()){
            holder.parking.setText("Oui");
        }else {
            holder.parking.setText("Non");
        }*/

        return v;
    }
}
