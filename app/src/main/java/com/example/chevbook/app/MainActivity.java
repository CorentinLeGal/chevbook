package com.example.chevbook.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.app.AlertDialog;
import android.widget.EditText;
import android.widget.Toast;

import java.lang.Object;


public class MainActivity extends ActionBarActivity {
    private AsyncTask<String, String, Boolean> mthreadcon = null;
    private EditText id;
    private EditText pass;
    private SharedPreferences myPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        id = (EditText)findViewById(R.id.id);
        pass = (EditText)findViewById(R.id.pass);

        myPrefs = this.getSharedPreferences("infoconnexion", 0);
        final String snom = myPrefs.getString("snom", "nothing");
        final String smdp = myPrefs.getString("smdp", "nothing");

        final Button Button = (Button) findViewById(R.id.btnok);
        Button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               // Intent intent = new Intent(MainActivity.this, Accueil.class);
               // startActivity(intent);

                String[] mesparams = { id.getText().toString(), pass.getText().toString(),"http://chevbook.charrierpaul.fr/android.php" };
                mthreadcon = new Connexion(MainActivity.this).execute(mesparams);

                SharedPreferences.Editor prefsEditor = myPrefs.edit();
                prefsEditor.putString("snom", id.getText().toString());
                prefsEditor.putString("smdp", pass.getText().toString());
                prefsEditor.commit();

            }
        });
    }


    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void alertmsg(String title, String msg) {
        AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle(title);
        adb.setPositiveButton("Ok", null);
        adb.setMessage(msg);
        adb.show();
    }

    public void verif(String sb)
    {
        try {
            int verif = Integer.parseInt(sb);

            if(verif == 1)
            {
                Intent intent = new Intent(getApplicationContext(), Accueil.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(MainActivity.this, "Mauvaise combinaison Identifiant/Mot de passe",Toast.LENGTH_SHORT).show();
            }
        }
        catch(NumberFormatException nfe) {
        }
    }
}
