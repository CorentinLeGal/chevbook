package com.example.chevbook.app;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


public class AjoutAppart extends ActionBarActivity {

    private AsyncTask<String, String, Boolean> mthreadcon = null;

    private EditText titre;
    private EditText adresse;
    private EditText prix;
    private EditText colloc;
    private EditText surface;
    private EditText meuble;
    private EditText distance;
    private EditText internet;
    private EditText nbpiece;
    private EditText parking;
    private EditText presentation;
    private String id;
    private SharedPreferences myPrefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_appart);



        getActionBar().setTitle("Ajouter un Appartement");

        final Button Button = (Button) findViewById(R.id.btnnewappart);
        Button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Intent intent = new Intent(MainActivity.this, Accueil.class);
                // startActivity(intent);

                titre = (EditText)findViewById(R.id.titreajout);
                adresse = (EditText)findViewById(R.id.adrajout);
                prix = (EditText)findViewById(R.id.prixajout);
                Spinner colloc = (Spinner)findViewById(R.id.collocajout);
                final String collocation = colloc.getSelectedItem().toString();
                surface = (EditText)findViewById(R.id.surfajout);
                Spinner meuble = (Spinner)findViewById(R.id.meubleajout);
                final String meubler = meuble.getSelectedItem().toString();
                distance = (EditText)findViewById(R.id.distajout);
                Spinner net = (Spinner)findViewById(R.id.netajout);
                final String internet = net.getSelectedItem().toString();
                nbpiece = (EditText)findViewById(R.id.pieceajout);
                Spinner park = (Spinner)findViewById(R.id.parkingajout);
                final String parking = park.getSelectedItem().toString();
                presentation = (EditText)findViewById(R.id.presajout);
                myPrefs = getSharedPreferences("infoconnexion", 0);
                id = myPrefs.getString("snom", "nothing");

                String[] mesparams = {  titre.getText().toString(), adresse.getText().toString(), prix.getText().toString(), collocation.toString(), surface.getText().toString(), meubler.toString(), distance.getText().toString(), internet.toString(), nbpiece.getText().toString(), parking.toString(), presentation.getText().toString(), id.toString() };
                mthreadcon = new NewAppart(AjoutAppart.this).execute(mesparams);

                Intent intent = new Intent(AjoutAppart.this, Accueil.class);
                Toast.makeText(getApplicationContext(), "Appartement ajouté !", Toast.LENGTH_SHORT).show();
                startActivity(intent);

            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.ajout_appart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void verif(String sb)
    {
        try {
            int verif = Integer.parseInt(sb);

            if(verif == 1)
            {
                Intent intent = new Intent(getApplicationContext(), Accueil.class);
                startActivity(intent);
            }
            else
            {
                Toast.makeText(AjoutAppart.this, "Impossible de joindre", Toast.LENGTH_SHORT).show();
            }
        }
        catch(NumberFormatException nfe) {
        }
    }



}
