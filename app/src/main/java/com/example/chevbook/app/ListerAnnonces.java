package com.example.chevbook.app;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Valentin on 17/04/2014.
 */
public class ListerAnnonces extends AsyncTask<String, String, Boolean> {
    // Référence à l'activité qui appelle
    private WeakReference<Activity> mActivity = null;
    private String vclassactivity;
    private String StringResult = "";
    private String error = "";

    public ListerAnnonces (Activity pActivity) {
        mActivity = new WeakReference<Activity>(pActivity);
        //permet de récupérer la class de l'appelant
        vclassactivity=pActivity.getClass().toString();
    }
    @Override
    protected void onPreExecute () {// Au lancement, on envoie un message à l'appelant
        if(mActivity.get() != null)
            Toast.makeText(mActivity.get(), "Début de la connexion", Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onPostExecute (Boolean result) {


        if (mActivity.get() != null) {
            if(result){
                //Toast.makeText(mActivity.get(), "Connexion établie avec le serveur", Toast.LENGTH_SHORT).show();
                if (vclassactivity.contains("Accueil"))
                {
                    ((Accueil)mActivity.get()).setAppartInListview(StringResult);
                    //Toast.makeText(mActivity.get(), StringResult, Toast.LENGTH_SHORT).show();
                }
            }
            else{
                //Toast.makeText(mActivity.get(), "Impossible de joindre le serveur", Toast.LENGTH_SHORT).show();
                Toast.makeText(mActivity.get(), error, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected Boolean doInBackground (String... params) {
        /*String vurl = "";
        if (vclassactivity.contains("Accueil")) {
            vurl = params[0];
        }*/

        StringBuilder sb = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL("http://chevbook.charrierpaul.fr/Androappart.php"); //passée par paramètre
            urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoOutput(true);
            urlConnection.setConnectTimeout(5000);

            OutputStreamWriter out = new OutputStreamWriter(
                    urlConnection.getOutputStream());
            // selon l'activity appelante on peut passer des paramètres en JSON exemple
            if (vclassactivity.contains("Accueil"))
            {
                // Création objet jsonn clé valeur
                JSONObject jsonParam = new JSONObject();
                // Exemple Clé valeur utiles à notre application
                out.write(jsonParam.toString());
                out.flush();
            }
            out.close();

            // récupération du serveur
            int HttpResult = urlConnection.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream(), "utf-8"));
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                br.close();

                StringResult = sb.toString();

                return true;
            } else {
                error = "HttpURLConnection pas OK";
                return false;
            }

        // gestion des erreurs
        } catch (MalformedURLException e) {
            error = "pb url";
            return false;
        } catch (java.net.SocketTimeoutException e) {
            error = "time exception";
            return false;
        } catch (IOException e) {
            error = "Erreur internet inexistant";
            return false;
        }finally {
            if (urlConnection != null){
                urlConnection.disconnect();
            }
        }
    }

    @Override
    protected void onCancelled () {
        if(mActivity.get() != null)
            Toast.makeText(mActivity.get(), "Annulation", Toast.LENGTH_SHORT).show();
    }
}

