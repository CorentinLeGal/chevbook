package com.example.chevbook.app;

import java.io.Serializable;

/**
 * Created by Valentin on 24/04/2014.
 */
public class profile implements Serializable{

    public String getNomF() {
        return nomF;
    }

    public String setNomF(String nomF) {
        return nomF;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    private String nom, nomF, prenom, classe, sexe, age, mail, telephone;

    public profile(String nom, String nomF, String prenom, String classe, String sexe, String age, String mail, String telephone  ) {
        this.nom = nom;
        this.nomF = nomF;
        this.prenom = prenom;
        this.classe = classe;
        this.sexe = sexe;
        this.age = age;
        this.mail = mail;
        this.telephone = telephone;
    }
}
