package com.example.chevbook.app;

/**
 * Created by Valentin on 10/04/2014.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

public class NewAppart extends AsyncTask<String, String, Boolean> {

    // Référence à l'activité qui appelle
    private WeakReference<Activity> mActivity = null;
    private String vclassactivity;
    private StringBuilder sb = new StringBuilder();
    private String StringResult = "";

    public NewAppart (Activity pActivity) {
        mActivity = new WeakReference<Activity>(pActivity);
        //permet de récupérer la class de l'appelant
        vclassactivity=pActivity.getClass().toString();
    }
    @Override
    protected void onPreExecute () {// Au lancement, on envoie un message à l'appelant
        if(mActivity.get() != null)
            Toast.makeText(mActivity.get(), "Début de la connexion",Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onPostExecute (Boolean result) {
        if (mActivity.get() != null) {
            if(result){
                Toast.makeText(mActivity.get(), "Connexion établie avec le serveur", Toast.LENGTH_SHORT).show();
//pour exemple on appelle une méthode de l'appelant qui va gérer la fin ok du thread
                if (vclassactivity.contains("AjoutAppart"))
                {
                    ((AjoutAppart)mActivity.get()).verif(StringResult);
                }
            }
            else
                Toast.makeText(mActivity.get(), "Impossible de joindre le serveur", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected Boolean doInBackground (String... params) {// Exécution en arrière plan
        // on simule ici une activité de 2 sec ne sert à rien
        String vtitre = "", vadresse = "", vprix = "", vcolloc = "", vsurface = "", vmeuble = "", vdistance = "", vinternet = "", vnbpiece = "", vparking = "", vpresentation = "", vid="";
        if (vclassactivity.contains("AjoutAppart")) {
            vtitre = params[0];
            vadresse = params[1];
            vprix = params[2];
            vcolloc = params[3];
            vsurface = params[4];
            vmeuble = params[5];
            vdistance = params[6];
            vinternet = params[7];
            vnbpiece = params[8];
            vparking = params[9];
            vpresentation = params[10];
            vid= params[11];


        }

        try {
            StringBuilder sb = new StringBuilder();
            HttpURLConnection urlConnection = null;
            try {
                URL url = new URL("http://chevbook.charrierpaul.fr/android_ajoutAppart.php"); //passée par paramètre
                urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoOutput(true);
                urlConnection.setConnectTimeout(5000);
                OutputStreamWriter out = new OutputStreamWriter(
                        urlConnection.getOutputStream());
                // selon l'activity appelante on peut passer des paramètres en JSON exemple
                if (vclassactivity.contains("AjoutAppart"))
                {
                    // Création objet jsonn clé valeur
                    JSONObject jsonParam = new JSONObject();
                    // Exemple Clé valeur utiles à notre application
                    jsonParam.put("titreajout", vtitre);
                    jsonParam.put("adrajout", vadresse);
                    jsonParam.put("prixajout", vprix);
                    jsonParam.put("collocajout", vcolloc);
                    jsonParam.put("surfajout", vsurface);
                    jsonParam.put("meubleajout", vmeuble);
                    jsonParam.put("distajout", vdistance);
                    jsonParam.put("netajout", vinternet);
                    jsonParam.put("pieceajout", vnbpiece);
                    jsonParam.put("parkingajout", vparking);
                    jsonParam.put("presajout", vpresentation);
                    jsonParam.put("id", vid);


                    out.write(jsonParam.toString());
                    out.flush();
                }
                out.close();
                // récupération du serveur
                int HttpResult = urlConnection.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(
                            urlConnection.getInputStream(), "utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line);
                    }
                    br.close();

                    StringResult = sb.toString();

                    //String[] vstring0 = { "Reçu du serveur",sb.toString()};
                    String[] vstring0 = {sb.toString()};
                    publishProgress(vstring0);
                    // on se servira du sb dans la méthode onPostExecute pour appel de la gestion de 	fin de thread si ok
                } else {
                    String[] vstring0 = { "Erreur",urlConnection.getResponseMessage()};
                    publishProgress(vstring0);
                }
                // gestion des erreurs
            } catch (MalformedURLException e) {
                String[] vstring0 = { "Erreur", "Problème d'URL" };
                publishProgress(vstring0);
                return false;
            } catch (java.net.SocketTimeoutException e) {
                String[] vstring0 = { "Erreur", "Temps trop long" };
                publishProgress(vstring0);
                return false;
            } catch (IOException e) {
                String[] vstring0 = { "Erreur", "Problèmes IO" };
                publishProgress(vstring0);
                return false;
            } catch (JSONException e) {
                String[] vstring0 = { "Erreur", "Problèmes json" };
                publishProgress(vstring0);
                return false;
            } finally {
                if (urlConnection != null){
                    urlConnection.disconnect();
                }
            }

            Thread.sleep(2000);
        }catch(InterruptedException e) {
            return false;
        }
        return true;
    }

    /*@Override
    protected void onProgressUpdate (String... param) {
        // utilisation de on progress pour afficher des message pendant le doInBackground
        // ici pour exemple on appelle une méthode de l'appelant qui peut par exemple modifier son layout
        if (vclassactivity.contains("MainActivity"))
        {
            //((MainActivity)mActivity.get()).alertmsg(param[0],param[1]);
            ((MainActivity)mActivity.get()).alertmsg("Recu du serveur",param[1]);
        }
    }*/
    @Override
    protected void onCancelled () {
        if(mActivity.get() != null)
            Toast.makeText(mActivity.get(), "Annulation", Toast.LENGTH_SHORT).show();
    }
}
