package com.example.chevbook.app;

import java.io.Serializable;

/**
 * Created by Valentin on 17/04/2014.
 */
public class Annonce implements Serializable{

    private int id, prix, surface, distanceLycee, nbPiece, idPost;
    private String nom,adresse, present;
    private boolean collocation, meuble, internet, parking;

    public Annonce(int id, int prix, int surface, int distanceLycee, int nbPiece, int idPost, String nom, String adresse, String present, String collocation, String meuble, String internet, String parking) {
        this.id = id;
        this.prix = prix;
        this.surface = surface;
        this.distanceLycee = distanceLycee;
        this.nbPiece = nbPiece;
        this.idPost = idPost;
        this.nom = nom;
        this.adresse = adresse;
        this.present = present;

        if(collocation.equals("oui"))
        {
            this.collocation = true;
        }
        else {
            this.collocation = false;
        }

        if(meuble.equals("oui"))
        {
            this.meuble = true;
        }
        else {
            this.meuble = false;
        }

        if(internet.equals("oui"))
        {
            this.internet = true;
        }
        else {
            this.internet = false;
        }

        if(parking.equals("oui"))
        {
            this.parking = true;
        }
        else {
            this.parking = false;
        }


    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrix() {
        return prix;
    }

    public void setPrix(int prix) {
        this.prix = prix;
    }

    public int getSurface() {
        return surface;
    }

    public void setSurface(int surface) {
        this.surface = surface;
    }

    public int getDistanceLycee() {
        return distanceLycee;
    }

    public void setDistanceLycee(int distanceLycee) {
        this.distanceLycee = distanceLycee;
    }

    public int getNbPiece() {
        return nbPiece;
    }

    public void setNbPiece(int nbPiece) {
        this.nbPiece = nbPiece;
    }

    public int getIdPost() {
        return idPost;
    }

    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public boolean isCollocation() {
        return collocation;
    }

    public void setCollocation(boolean collocation) {
        this.collocation = collocation;
    }

    public boolean isMeuble() {
        return meuble;
    }

    public void setMeuble(boolean meuble) {
        this.meuble = meuble;
    }

    public boolean isInternet() {
        return internet;
    }

    public void setInternet(boolean internet) {
        this.internet = internet;
    }

    public boolean isParking() {
        return parking;
    }

    public void setParking(boolean parking) {
        this.parking = parking;
    }
}
