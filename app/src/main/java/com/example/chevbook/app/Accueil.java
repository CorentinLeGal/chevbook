package com.example.chevbook.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class Accueil extends ActionBarActivity {

    ListViewAppartAdapter adapter;
    private ArrayList<Annonce> list = new ArrayList<Annonce>();
    private AsyncTask<String, String, Boolean> mthreadlist = null;
    private ListView listViewAppart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);

        final ImageButton Button = (ImageButton) findViewById(R.id.btnadd);
        Button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Accueil.this, AjoutAppart.class);
                startActivity(intent);
            }
        });

        final ImageButton Button2 = (ImageButton) findViewById(R.id.btnsearch);
        Button2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Accueil.this, SearchAppart.class);
                startActivity(intent);
            }
        });

        final ImageButton Button3 = (ImageButton) findViewById(R.id.btnprofil);
        Button3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Accueil.this, MyProfil.class);
                startActivity(intent);
            }
        });

        final ImageButton Button4 = (ImageButton) findViewById(R.id.btnhelp);
        Button4.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Accueil.this, Help.class);
                startActivity(intent);
            }
        });

        listViewAppart = (ListView) findViewById(R.id.listViewAppart);

        /*adapter = new ListViewAppartAdapter(getBaseContext());
        listViewAppart.setAdapter(adapter);*/
        String[] mesparams = {"http://chevbook.charrierpaul.fr/Androappart.php"};
        mthreadlist = new ListerAnnonces(Accueil.this).execute(mesparams);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.accueil, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void setAppartInListview(String sb) {
        //todo
        /*adapter = new ListViewAppartAdapter(getBaseContext());
        listViewAppart.setAdapter(adapter);*/
        //Toast.makeText(getApplicationContext(), sb, Toast.LENGTH_SHORT).show();

        try {
            JSONArray jsonArray = new JSONArray(sb);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                list.add(new Annonce(jsonObject.getInt("id"), jsonObject.getInt("prix"), jsonObject.getInt("surface"), jsonObject.getInt("distanceLycee"), jsonObject.getInt("nbPiece"), jsonObject.getInt("idPost"), jsonObject.getString("nom"), jsonObject.getString("adresse"), jsonObject.getString("present"), jsonObject.getString("collocation"), jsonObject.getString("meuble"), jsonObject.getString("internet"), jsonObject.getString("parking")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        adapter = new ListViewAppartAdapter(getBaseContext(), list);
        listViewAppart.setAdapter(adapter);

        listViewAppart.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), AffichAppart.class);
                intent.putExtra("Annonce", list.get(position));
                startActivity(intent);
            }
        });
    }
}
