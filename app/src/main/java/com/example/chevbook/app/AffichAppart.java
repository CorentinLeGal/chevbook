package com.example.chevbook.app;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;


public class AffichAppart extends ActionBarActivity {

    private Annonce annonce;
    public TextView adresse;
    public TextView prix;
    public TextView colloc;
    public TextView surface;
    public TextView meuble;
    public TextView distance;
    public TextView internet;
    public TextView nbpiece;
    public TextView parking;
    public TextView presentation;
    public TextView titre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affich_appart);

        annonce = (Annonce)getIntent().getSerializableExtra("Annonce");

        titre = (TextView) findViewById(R.id.TVtitreannonce);
        adresse = (TextView) findViewById(R.id.TVadr);
        prix = (TextView) findViewById(R.id.TVprix);
        colloc = (TextView) findViewById(R.id.TVcolloc);
        surface = (TextView) findViewById(R.id.TVsurface);
        meuble = (TextView) findViewById(R.id.TVmeuble);
        distance = (TextView) findViewById(R.id.TVdist);
        internet = (TextView) findViewById(R.id.TVnet);
        nbpiece = (TextView) findViewById(R.id.TVnbpiece);
        parking = (TextView) findViewById(R.id.TVparking);
        presentation = (TextView) findViewById(R.id.TVpresention);



        titre.setText(annonce.getNom());
        adresse.setText(annonce.getAdresse());
        prix.setText(Integer.toString(annonce.getPrix()));

        if (annonce.isCollocation()){
            colloc.setText("Oui");
        }else {
            colloc.setText("Non");
        }

        surface.setText(Integer.toString(annonce.getSurface()));

        if (annonce.isMeuble()){
            meuble.setText("Oui");
        }else {
            meuble.setText("Non");
        }

        distance.setText(Integer.toString(annonce.getDistanceLycee()));

        if (annonce.isInternet()){
            internet.setText("Oui");
        }else {
            internet.setText("Non");
        }

        nbpiece.setText(Integer.toString(annonce.getNbPiece()));

        if (annonce.isParking()){
            parking.setText("Oui");
        }else {
            parking.setText("Non");
        }

        presentation.setText(annonce.getPresent());


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.affich_appart, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
